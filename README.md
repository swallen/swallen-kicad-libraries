# User KiCad Libraries

This repository contains all of the KiCad models I've written for a variety of personal projects.
For the most part the libraries have been migrated to KiCad 6.

This repository is intended to be added as a submodule to another repository, but of course can be cloned separately.

All libraries are appended with a "u" to clearly separate them from built-in KiCad libraries.